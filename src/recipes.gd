extends Node


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var lettuceburger = ["choppedlettuce", "bun", "cookedpatty"]
var tomatoburger = ["choppedtomato", "bun", "cookedpatty"]
var tomatosalad = ["choppedtomato", "choppedlettuce"]
var fishnchips = ["friedfish", "fries"]
var chickensalad = ["choppedlettuce", "cookedchicken"]
var veggiesandwhich = ["bun", "choppedlettuce", "choppedtomato"]
var cheeseburger = ["bun", "cookedpatty", "slicedcheese"]
var alfredo = ["cookednoodles", "slicedcheese"]
var chickenalfredo = ["cookednoodles", "slicedcheese", "cookedchicken"]
var pasta = ["cookednoodles", "cookedtomatos"]
var friesnketchup = ["fries", "cookedtomatos"]
var chickensandwhich = ["bun", "choppedlettuce", "cookedchicken"]

var foods = ["lettuceburger", "tomatosalad", "tomatoburger", "fishnchips", "chickensalad", "veggiesandwhich", "cheeseburger", "alfredo", "chickenalfredo", "pasta", "friesnketchup", "chickensandwhich"]
var recipes = [lettuceburger, tomatosalad, tomatoburger, fishnchips, chickensalad, veggiesandwhich, cheeseburger, alfredo, chickenalfredo, pasta, friesnketchup, chickensandwhich]

var jank_foods = ["fries"]
var jank_recipes = [["choppedpotato"]]

var all_foods = ["lettuceburger", "tomatosalad", "fries", "tomatoburger", "fishnchips", "chickensalad", "veggiesandwhich", "cheeseburger", "alfredo", "chickenalfredo", "pasta", "friesnketchup", "chickensandwhich"]
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
