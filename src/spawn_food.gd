extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var time = 0
var food_scene = preload("res://src/food.tscn")

var rng = RandomNumberGenerator.new()
var wait_time = rng.randi_range(1, 4)

var list = ["balllettuce", "balllettuce", "bun", "bun", "rawpatty", "rawpotato", "rawpotato", "rawtomato", "rawtomato", "fish", "chicken", "cheese", "noodles"]



func spawn():
	var food = food_scene.instance()
	get_parent().add_child(food)
	var food_rng = rng.randi_range(0, list.size()-1)
	food.food_type = (list[food_rng])
	food.global_position = global_position
# Called when the node enters the scene tree for the first time.
func _ready():
	rng.randomize()
	pass # Replace with function body.
func _process(delta):
	time += delta
	if(time > wait_time):
		spawn()
		wait_time = rng.randi_range(1, 2)
		time = 0

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
