extends Area2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
onready var place_sound = get_node("placeSound")
onready var drop_sound = get_node("dropSound")
var holding = false
var grabbed_item 
var time = 0
#interact code, is gross and could be cleaned up but also it works soooo
func interact():	
	#if we hit interact key, and there is something to interact with
	if (get_overlapping_areas().size() > 0 ):
		#if holding is false than we check to make sure its type is valid to be picked up
		if(holding == false):
			var areas = get_overlapping_areas()
			areas.invert()
			for area in areas:
				if area.has_method("get_type"):
					if(area.get_type() != null and area.is_held == false and area.can_grab(1) == true):
						area.picked_up()
						place_sound.play()
						grabbed_item = area
						holding = true
						for area2 in areas:
							if area2.has_method("item_removed"):
								area2.item_removed()
						break
				
				
			#else if we are already holding something then we check to make sure its a location we can drop it in, and if so than drop it and clear the item we are holding
		elif(holding == true):
			for area in get_overlapping_areas():
				if area.has_method("get_food_loc"):
					if(grabbed_item != null):
						if(area.get_food_loc(grabbed_item) == true):
							holding = false
							grabbed_item.dropped(1)
							grabbed_item.set_global_position(area.global_position)
							if area.has_method("get_food_destroy"):
								if area.get_food_destroy(grabbed_item) == true:
									grabbed_item.queue_free()
									drop_sound.play()
							if area.has_method("food_placed"):
								area.food_placed()
								drop_sound.play()
							grabbed_item = null
							drop_sound.play()
							holding = false
							break
						
func transform_food():
	for area in get_overlapping_areas():
		if area.has_method("transform"):
			area.transform()
			break

func button_time(delta):
	if(Input.is_action_pressed("interactCon")):
		time += delta
	
	if(Input.is_action_just_pressed("interact")):
		interact()
		time = 0
	if(Input.is_action_pressed("interactCon") and time > 15.0/60.0):
		if(holding == false):
			transform_food()
		time = 0
	
func _physics_process(delta):
	if grabbed_item == null:
		holding = false
	if holding == false:
		grabbed_item = null
	button_time(delta)
	#just moves the item we have to be in front of us for "aesthetics"
	if (grabbed_item != null):
		if(is_instance_valid(grabbed_item)):
			grabbed_item.set_global_position(global_position)
	

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
