extends Node


var player1recipes = ["1", "2", "3"]
var player2recipes = ["1", "2", "3"]
var rng = RandomNumberGenerator.new()

func newrecipe(player, replace):
	if(player == 1):
		var num = player1recipes.find(replace)
		player1recipes[num] = (Recipes.all_foods[rng.randi_range(0, Recipes.all_foods.size()-1)])
	elif(player == 2):
		var num = player2recipes.find(replace)
		player2recipes[num] = (Recipes.all_foods[rng.randi_range(0, Recipes.all_foods.size()-1)])
var player1score = 0
var player2score = 0

func _ready():
	rng.randomize()
	newrecipe(1, "1")
	newrecipe(1, "2")
	newrecipe(1, "3")
	
	newrecipe(2, "1")
	newrecipe(2, "2")
	newrecipe(2, "3")
