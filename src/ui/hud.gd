extends Node2D

var player1recipe1
var player1recipe2
var player1recipe3

var player2recipe1
var player2recipe2
var player2recipe3

var player1score
var player2score

func _ready():
	player1recipe1 = $Player1Recipe1
	player1recipe2 = $Player1Recipe2
	player1recipe3 = $Player1Recipe3
	
	player2recipe1 = $Player2Recipe1
	player2recipe2 = $Player2Recipe2
	player2recipe3 = $Player2Recipe3
	
	player1score = $Player1Score
	player2score = $Player2Score

func _process(delta):
	if Scores.player1score != player1score.score:
		player1score.set_score(Scores.player1score)
	if Scores.player2score != player2score.score:
		player2score.set_score(Scores.player2score)
	
	if Scores.player1recipes[0] != player1recipe1.recipe:
		player1recipe1.set_recipe(Scores.player1recipes[0])
	if Scores.player1recipes[1] != player1recipe2.recipe:
		player1recipe2.set_recipe(Scores.player1recipes[1])
	if Scores.player1recipes[2] != player1recipe3.recipe:
		player1recipe3.set_recipe(Scores.player1recipes[2])
	
	if Scores.player2recipes[0] != player2recipe1.recipe:
		player2recipe1.set_recipe(Scores.player2recipes[0])
	if Scores.player2recipes[1] != player2recipe2.recipe:
		player2recipe2.set_recipe(Scores.player2recipes[1])
	if Scores.player2recipes[2] != player2recipe3.recipe:
		player2recipe3.set_recipe(Scores.player2recipes[2])
