extends Node2D


var score := 0

func _ready():
	set_score(24)

func set_score(val):
	score = val
	var ones = val % 10
	var tens = floor(val / 10)
	if $Tens != null:
		$Tens.text = str(tens)
	if $Ones != null:
		$Ones.text = str(ones)
