extends Node2D


export var recipe: String

func _ready():
	set_recipe(recipe)

func set_recipe(val):
	recipe = val
	$AnimationPlayer.play("NewRecipe")

func set_sprite(sprite, food):
	if sprite != null:
		var path = "res://assets/food/" + food + ".png"
		if ResourceLoader.exists(path):
			sprite.texture = load(path)

func show_recipe():
	var ingredients = []
	if Recipes.foods.has(recipe):
		ingredients = Recipes.recipes[Recipes.foods.find(recipe)]
	if Recipes.jank_foods.has(recipe):
		ingredients = Recipes.jank_recipes[Recipes.jank_foods.find(recipe)]
	set_sprite($Card/Target, recipe)
	$Card/Ingredient1.visible = false
	$Card/Ingredient2.visible = false
	$Card/Ingredient3.visible = false
	if ingredients.size() > 0:
		set_sprite($Card/Ingredient1, ingredients[0])
		$Card/Ingredient1.visible = true
	if ingredients.size() > 1:
		set_sprite($Card/Ingredient2, ingredients[1])
		$Card/Ingredient2.visible = true
	if ingredients.size() > 2:
		set_sprite($Card/Ingredient3, ingredients[2])
		$Card/Ingredient3.visible = true
