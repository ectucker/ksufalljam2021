extends Node2D


var bar: TextureProgress


func _ready():
	bar = find_node("TextureProgress")

func set_progress(amount):
	if amount <= 0 or amount >= 1:
		bar.visible = false
	else:
		bar.visible = true
	bar.value = amount
