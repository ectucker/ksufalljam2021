extends AnimationPlayer


export var input: String


func _input(event):
	if event.is_action_pressed(input):
		queue("Show")
	elif event.is_action_released(input):
		queue("Hide")
