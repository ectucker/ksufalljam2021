extends Area2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
onready var fry = get_node("fry")

var fryable = ["choppedpotato", "fish", "chicken"]
var cook_time = 0
var food_in_fryer = false
func get_food_loc(food):
	if(food.has_method("get_type") and food_in_fryer == false):
		for items in fryable:
			if(items == food.get_type()):
				return true
	return false

func is_cooking(delta):
	
	if(food_in_fryer == true):
		if(!fry.is_playing()):
			fry.play()
		cook_time += delta
	if(cook_time > 10):
		finished_product()
		cook_time = 0
	$ProgressBar.set_progress(cook_time / 10.0)

func item_removed():
	cook_time = 0
	food_in_fryer = false
	fry.stop()

func food_placed():
	food_in_fryer = true

func finished_product():
	for area in get_overlapping_areas():
		if(area.has_method("get_type")):
			var type = area.get_type()
			match type:
				"fries":
					area.food_type = "burned"
					return
				"choppedpotato":
					area.food_type = "fries"
					return
				"fish":
					area.food_type = "friedfish"
					return
				"chicken":
					area.food_type = "cookedchicken"
					return
			area.food_type = "burned"
				
					
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _process(delta):
	is_cooking(delta)

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
