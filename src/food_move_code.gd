extends Area2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


func move():
	if(get_overlapping_areas().size() > 0):
		for area in get_overlapping_areas():
			if(area.has_method("transport")):
				area.transport()
				
				
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _process(delta):
	move()
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
