extends Area2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
onready var cook = get_node("cook")
var cookable = ["rawpatty", "choppedtomato", "noodles"]

var cook_time = 0
var food_on_stove = false

func get_food_loc(food):
	if(food.has_method("get_type")):
		for items in cookable:
			if(items == food.get_type() and food_on_stove == false):
				return true
	return false

func item_removed():
	cook_time = 0
	food_on_stove = false
	cook.stop()
	
func is_cooking(delta):
	if(food_on_stove == true):
		if(!cook.is_playing()):
			cook.play()
		cook_time += delta
	if(cook_time > 10):
		finished_product()
		cook_time = 0
	$ProgressBar.set_progress(cook_time / 10.0)
	
		
func food_placed():
	food_on_stove = true

func finished_product():
	for area in get_overlapping_areas():
		if(area.has_method("get_type")):
			var type = area.get_type()
			match type:
				"cookedpatty":
					area.food_type = "burned"
					return
				"rawpatty":
					area.food_type = "cookedpatty"
					return
				"choppedtomato":
					area.food_type = "cookedtomatos"
					return
				"noodles":
					area.food_type = "cookednoodles"
					return
			area.food_type = "burned"
				

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.
func _process(delta):
	is_cooking(delta)

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
