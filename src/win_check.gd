extends Node2D


var ended = false


export var player1path: NodePath
export var player2path: NodePath

var player_1
var player_2

func _ready():
	player_1 = get_node(player1path)
	player_2 = get_node(player2path)

func _process(delta):
	if not ended:
		if Scores.player1score >= 25:
			ended = true
			eat(player_2)
		elif Scores.player2score >= 25:
			ended = true
			eat(player_1)

func eat(player):
	player.freeze = true
	var tween = Tween.new()
	add_child(tween)
	var target = player.global_position
	target.x = 0
	var move1time = (player.global_position - target).length() / (0.4 * 60)
	tween.interpolate_property(player, "global_position",
		player.global_position, target, move1time,
		Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	var target2 = Vector2(0, 0)
	var move2time = (target - target2).length() / (0.4 * 60)
	tween.interpolate_property(player, "global_position",
		target, target2, move2time,
		Tween.TRANS_LINEAR, Tween.EASE_IN_OUT,
		move1time)
	tween.start()
