extends Area2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
onready var chopping_sound = get_node("AudioStreamPlayer2D")
var cuttable = ["balllettuce", "rawtomato", "rawpotato", "cheese"]
var count = 0
var item_on_board = false
func get_food_loc(food):
	if(food.has_method("get_type")):
		for items in cuttable:
			if(items == food.get_type() and item_on_board == false):
				return true
	return false

func item_removed():
	count = 0
	$ProgressBar.set_progress(float(count) / 6)
	item_on_board = false
	chopping_sound.stop()
	print("item on board false")
	
func food_placed():
	item_on_board = true

func transform():
	if(get_overlapping_areas().size() > 0):
		for area in get_overlapping_areas():
			if(area.has_method("get_type")):
				for items in cuttable:
					if(items == area.get_type()):
						if(!chopping_sound.is_playing()):
							chopping_sound.play()
						count += 1
						if(count > 5):
							count = 0
							chopping_sound.stop()
							finished_product()
						$ProgressBar.set_progress(float(count) / 6)
						
func finished_product():
	for area in get_overlapping_areas():
		if(area.has_method("get_type")):
			var type = area.get_type()
			match type:
				"balllettuce":
					area.food_type = "choppedlettuce"
				"rawtomato":
					area.food_type = "choppedtomato"
				"rawpotato":
					area.food_type = "choppedpotato"
				"cheese":
					area.food_type = "slicedcheese"
				

					
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
