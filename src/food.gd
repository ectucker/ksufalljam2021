tool
extends Area2D


export var food_type: String setget set_type
var velocity = Vector2()
var speed = 0.4
var is_held = false
var player_num

func _ready():
	set_type(food_type)

func can_grab(num):
	if(player_num != null):
		if(num == player_num):
			return true
		else:
			return false
	return true

func picked_up():
	is_held = true

func dropped(player):
	is_held = false
	print("is held is false")
	player_num = player
	


func get_type():
	return food_type

func set_type(type):
	food_type = type
	if $Sprite != null:
		var sprite = $Sprite
		if type == "burned":
			sprite.material = sprite.material.duplicate()
			sprite.material.set_shader_param("burn", sprite.material.get_shader_param("burn") * 0.5)
		else:
			var path = "res://assets/food/" + type + ".png"
			if ResourceLoader.exists(path):
				sprite.texture = load(path)

func transport():
	global_position.y -= 1 * speed

func _physics_process(delta):
	var overlapping = []
	for area in get_overlapping_areas():
		if area.has_method("get_type") and area.global_position == global_position:
			overlapping.append(area)
	overlapping.append(self)
	var offset = 0
	for food in overlapping:
		food.get_node("Sprite").offset.y = offset
		offset -= 6
