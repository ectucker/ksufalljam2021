extends Node


var target


func transition_to(scene):
	target = scene
	$AnimationPlayer.play("Transition")

func anim_trigger():
	get_tree().change_scene(target)
