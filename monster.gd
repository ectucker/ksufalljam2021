extends Area2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
onready var happy = get_node("happy")
onready var sad = get_node("sad")
onready var points_up = get_node("points_up")
onready var points_down = get_node("points_down")
func get_score(type):
	match type:
		"lettuceburger":
			return 5
		"fries":
			return 3
		"tomatosalad":
			return 4
		"tomatoburger":
			return 5
		"fishnchips":
			return 4
		"chickensalad":
			return 4
		"chickensandwhich":
			return 4
		"veggiesandwhich":
			return 5
		"chicken strips":
			return 3
		"pasta":
			return 5
		"cheeseburger":
			return 5
		"alfredo":
			return 4
		"chickenalfredo":
			return 5
		
	
	return -1

func is_recipe(type, player):
	if(player == 1):
		for item in Scores.player1recipes:
			if(type == item):
				#Scores.player1recipes.erase(item)
				Scores.newrecipe(1, item)
				return true
	elif(player == 2):
		for item in Scores.player2recipes:
			if(type == item):
				#Scores.player2recipes.erase(item)
				Scores.newrecipe(2, item)
				return true
	return false
func eat():
	if(get_overlapping_areas().size() > 0):
		var score = 0
		for area in get_overlapping_areas():
			if "player_num" in area:
				if(area.player_num == 1):
					if(is_recipe(area.food_type, 1) == true):
						score = get_score(area.food_type)
						if(score > 1):
							happy.play()
							points_up.play()
						elif(score < 1):
							sad.play()
							points_down.play()
						Scores.player1score += score
						area.queue_free()
					else:
						Scores.player1score -= 1
						sad.play()
						points_down.play()
						area.queue_free()
				elif(area.player_num == 2):
					if(is_recipe(area.food_type, 2) == true):
						score = get_score(area.food_type)
						if(score > 1):
							happy.play()
							points_up.play()
						elif(score < 1):
							sad.play()
							points_down.play()
						Scores.player2score += score
						area.queue_free()
					else:
						Scores.player2score -= 1
						sad.play()
						points_down.play()
						area.queue_free()
				else:
					area.queue_free()
	if get_overlapping_bodies().size() > 0:
		$Burp.play()
		Scores.player1score = 0
		Scores.player2score = 0
		for body in get_overlapping_bodies():
			body.queue_free()
			Transition.transition_to("res://MainMenu.tscn")
# Called when the node enters the scene tree for the first time.
func _ready():
	Scores.player1score = 0
	Scores.player2score = 0
func _process(delta):
	eat()
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
