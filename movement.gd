extends KinematicBody2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export (int) var speed = 200

var velocity = Vector2()
var rotation_dir = 0
var last_rot = 0
var freeze = false
#movement section
#also holds rotation code
func get_input():
	velocity = Vector2()
	if Input.is_action_pressed("right"):
		last_rot = 0
		velocity.x += 1
	if Input.is_action_pressed("left"):
		last_rot = PI
		velocity.x -= 1
	if Input.is_action_pressed("down"):
		last_rot = (PI) / 2
		velocity.y += 1
	if Input.is_action_pressed("up"):
		last_rot = (PI*3) / 2
		velocity.y -= 1
	
	#get a vel off of our vel normalized
	velocity = velocity.normalized() * speed
	#if we are not moving set last direction to last key pressed, otherwise point us towards direction moving
	if(velocity.x == 0 and velocity.y == 0):
		rotation_dir = last_rot
	else:
		rotation_dir = atan2(velocity.y, velocity.x)

	
func _physics_process(delta):
	get_input()
	if not freeze:
		velocity = move_and_slide(velocity)
	rotation = rotation_dir

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
