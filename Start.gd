extends Button


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

func start_game():
	Transition.transition_to("res://src/main.tscn")
# Called when the node enters the scene tree for the first time.
func _ready():
	connect("pressed", self, "start_game")
	OS.window_fullscreen = true


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
