extends Area2D

onready var sound = get_node("combineSound")
# Declare member variables here. Examples:
# var a = 2
# var b = "text"

#func that takes in a food
# checks if it has a type, and if the food type does exist (is a food)
# than we return that we can place the food here as a valid spot
func get_food_loc(food):
	if food != null:
		if is_instance_valid(food):
			if(food.has_method("get_type")):
				if(food.get_type() != null):
					return true
# Called when the node enters the scene tree for the first time.
#DEAR GOD IDK IT WORKS
func transform():
	var types = []
	var foodNodes = []
	
	for area in get_overlapping_areas():
		if area.has_method("get_type"):
			types.append(area.get_type())
			foodNodes.append(area)
	for i in range(0, Recipes.recipes.size()):
		var foodRecipe = Recipes.recipes[i]
		var toCompare = types.duplicate()
		var toMatch = foodRecipe.size()
		var matches = 0
		for strings in foodRecipe:
			for items in toCompare:
				if(items == strings):
					toCompare.erase(items)
					matches += 1
		if(toCompare.size() == 0 and toMatch == matches):
			var target = Recipes.foods[i]
			for j in range(1, foodNodes.size()):
				sound.play()
				foodNodes[j].queue_free()
			foodNodes[0].food_type = target

func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
